package com.ashu.orderservice.api.service;

import com.ashu.orderservice.api.common.Payment;
import com.ashu.orderservice.api.common.TransactionRequest;
import com.ashu.orderservice.api.common.TransactionResponse;
import com.ashu.orderservice.api.entity.Order;
import com.ashu.orderservice.api.repository.OrderRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RefreshScope
public class OrderService {

    @Autowired
    private final OrderRepository orderRepository;

    @Autowired
    @Lazy
    private RestTemplate restTemplate;

    @Value("${microservice.payment-service.endpoints.endpoint.uri}")
    private String PAYMENT_ENDPOINT;
    private Logger logger = LoggerFactory.getLogger(Order.class);

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public TransactionResponse saveOrder(TransactionRequest request) throws JsonProcessingException {
        String message = null;
        Order order = request.getOrder();
        Payment payment = request.getPayment();
        payment.setOrderId(order.getId());
        payment.setAmount(order.getPrice());
        logger.info("OrderService : {}", new ObjectMapper().writeValueAsString(request));
        Payment paymentResponse = restTemplate.postForObject(PAYMENT_ENDPOINT,payment,Payment.class);
        logger.info("PaymentService response for OrderService request : {}", new ObjectMapper().writeValueAsString(paymentResponse));
        message = paymentResponse.getPaymentStatus().equals("success")?"Payment successful,order placed":"Payment unsuccessful,order is in the cart";
        return new TransactionResponse(order,paymentResponse.getTransactionId(),paymentResponse.getAmount(),message);
    }
}
